
/*
 * Copyright (C) Igor Sysoev
 * Copyright (C) Nginx, Inc.
 */


#include <ngx_config.h>
#include <ngx_core.h>


#if (( __i386__ || __amd64__ ) && ( __GNUC__ || __INTEL_COMPILER ))//条件编译指令，检查当前的编译器是否支持i386或amd64架构，并且是GNU C编译器或着intel的编译器，若满足这些条件即执行后需代码


static ngx_inline void ngx_cpuid(uint32_t i, uint32_t *buf);//对函数ngx_cpuid的声明，ngx_cpuid函数的作用是使用CPUID指令来获取CPU的信息


#if ( __i386__ )

static ngx_inline void
ngx_cpuid(uint32_t i, uint32_t *buf)//对函数ngx_cpuid的声明，ngx_cpuid函数的作用是使用CPUID指令来获取CPU的信息
{

    /*
     * we could not use %ebx as output parameter if gcc builds PIC,
     * and we could not save %ebx on stack, because %esp is used,
     * when the -fomit-frame-pointer optimization is specified.
     */

    __asm__ (

    "    mov    %%ebx, %%esi;  "

    "    cpuid;                "
    "    mov    %%eax, (%1);   "
    "    mov    %%ebx, 4(%1);  "
    "    mov    %%edx, 8(%1);  "
    "    mov    %%ecx, 12(%1); "

    "    mov    %%esi, %%ebx;  "

    : : "a" (i), "D" (buf) : "ecx", "edx", "esi", "memory" );
}


#else /* __amd64__ */


static ngx_inline void
ngx_cpuid(uint32_t i, uint32_t *buf)//实现现CPUID指令，它将CPU的详细信息存储在eax, ebx, ecx, edx寄存器中
{
    uint32_t  eax, ebx, ecx, edx;

    __asm__ (

        "cpuid"

    : "=a" (eax), "=b" (ebx), "=c" (ecx), "=d" (edx) : "a" (i) );

    buf[0] = eax;
    buf[1] = ebx;
    buf[2] = edx;
    buf[3] = ecx;//将cpu的信息储存到buf数组中
}


#endif


/* auto detect the L2 cache line size of modern and widespread CPUs */

void  ngx_cpuinfo(void)//获取CPU的详细信息,并判断CPU的类型。如果CPU是Intel的，它会进一步判断CPU的型号，并根据不同的型号设定Cache Line Size。Cache Line Size主要是用于优化内存访问和提高性能。
{
    u_char    *vendor; //定义一个指向字符数组的指针，用于存储CPU的供应商信息
    uint32_t   vbuf[5], cpu[4], model;//定义三个uint32_t类型的变量。vbuf用于存储CPUID获取到的信息，cpu用于存储CPU的型号，model用于存储CPU的模型。

    vbuf[0] = 0;
    vbuf[1] = 0;

    vbuf[2] = 0;
    vbuf[3] = 0;
    vbuf[4] = 0;//对vbuf数组初始化全部置为0

    ngx_cpuid(0, vbuf);//调用获得cpu的信息

    vendor = (u_char *) &vbuf[1];//将vbuf数组初始化为CPUID获取到的信息。vbuf[1]赋值给vendor指针，因为在这个数组中，第二个元素存储的是供应商ID。

    if (vbuf[0] == 0) {
        return;
    }//如果为了说明信息未获取成功函数返回结束执行

    ngx_cpuid(1, cpu);//传入1获取cpu的型号

    if (ngx_strcmp(vendor, "GenuineIntel") == 0) {

        switch ((cpu[0] & 0xf00) >> 8) {//从CPU的型号中提取信息取出CPU型号的第9位

        /* Pentium */
        case 5:
            ngx_cacheline_size = 32;//如果第9位是5，设定Cache Line Size为32
            break;

        /* Pentium Pro, II, III */
        case 6:
            ngx_cacheline_size = 32; //如果第9位是6，设定Cache Line Size为32

            model = ((cpu[0] & 0xf0000) >> 8) | (cpu[0] & 0xf0);

            if (model >= 0xd0) {
                /* Intel Core, Core 2, Atom */
                ngx_cacheline_size = 64;
            }//模型大于或等于0xd0（十进制的130），则设定Cache Line Size为64

            break;

        /*
         * Pentium 4, although its cache line size is 64 bytes,
         * it prefetches up to two cache lines during memory read
         */
        case 15:
            ngx_cacheline_size = 128;
            break;
        }

    } else if (ngx_strcmp(vendor, "AuthenticAMD") == 0) {
        ngx_cacheline_size = 64;//如果CPU的vendor与字符串"AuthenticAMD"等于，则将ngx_cacheline_size设置为64
    }
}

#else


void
ngx_cpuinfo(void)
{
}


#endif
