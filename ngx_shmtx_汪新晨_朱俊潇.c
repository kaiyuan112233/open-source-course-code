
/*
 * Copyright (C) Igor Sysoev
 * Copyright (C) Nginx, Inc.
 */


#include <ngx_config.h>
#include <ngx_core.h>


#if (NGX_HAVE_ATOMIC_OPS)// 如果宏 NGX_HAVE_ATOMIC_OPS 被定义,表示系统支持原子操作

static void ngx_shmtx_wakeup(ngx_shmtx_t *mtx);// 前置声明静态函数 ngx_shmtx_wakeup


ngx_int_t
ngx_shmtx_create(ngx_shmtx_t *mtx, ngx_shmtx_sh_t *addr, u_char *name)
{
    mtx->lock = &addr->lock;// mtx->lock 指向共享内存地址 addr 中的锁变量

    if (mtx->spin == (ngx_uint_t) -1) {
        return NGX_OK;
    }

    mtx->spin = 2048;  // mtx->spin 用于自旋锁

#if (NGX_HAVE_POSIX_SEM)

    mtx->wait = &addr->wait;// mtx->wait 等待进程数,mtx->sem 信号量

    if (sem_init(&mtx->sem, 1, 0) == -1) {
        ngx_log_error(NGX_LOG_ALERT, ngx_cycle->log, ngx_errno,
                      "sem_init() failed");
    } else {
        mtx->semaphore = 1;
    }

#endif

    return NGX_OK;
  // 返回成功或错误码 
}


void
ngx_shmtx_destroy(ngx_shmtx_t *mtx)
{ // 销毁名为 mtx 的共享互斥量
#if (NGX_HAVE_POSIX_SEM)
 // 如果系统支持 POSIX 信号量

    if (mtx->semaphore) { // 如果该互斥量使用了信号量

        if (sem_destroy(&mtx->sem) == -1) {
             // 销毁信号量失败
            ngx_log_error(NGX_LOG_ALERT, ngx_cycle->log, ngx_errno,
                          "sem_destroy() failed"); // 打印错误日志 
        }
    }

#endif
}


ngx_uint_t
ngx_shmtx_trylock(ngx_shmtx_t *mtx)
{// 尝试获取 mtx 锁,不阻塞
    return (*mtx->lock == 0 && ngx_atomic_cmp_set(mtx->lock, 0, ngx_pid)); // 如果获取成功返回 1,失败返回 0
}


void
ngx_shmtx_lock(ngx_shmtx_t *mtx)
{
    ngx_uint_t         i, n; // 打印调试日志


    ngx_log_debug0(NGX_LOG_DEBUG_CORE, ngx_cycle->log, 0, "shmtx lock");

    for ( ;; ) {

        if (*mtx->lock == 0 && ngx_atomic_cmp_set(mtx->lock, 0, ngx_pid)) { // 尝试获取锁
            return; // 如果获取成功则返回
        }

        if (ngx_ncpu > 1) { // 在多核CPU上使用自旋锁

            for (n = 1; n < mtx->spin; n <<= 1) {// 自旋次数指数递增

                for (i = 0; i < n; i++) {// 在循环中让出CPU
                    ngx_cpu_pause();
                }

                if (*mtx->lock == 0// 再次尝试获取锁
                    && ngx_atomic_cmp_set(mtx->lock, 0, ngx_pid))
                {
                    return;// 如果获取成功则返回
                }
                // 如果是单核CPU,或者自旋获取锁失败
                  // 则通过信号量阻塞等待锁
            }
        }

#if (NGX_HAVE_POSIX_SEM)
// 如果支持POSIX信号量
        if (mtx->semaphore) {// 如果该互斥量使用了信号量

            (void) ngx_atomic_fetch_add(mtx->wait, 1);// 原子增加等待进程数

            if (*mtx->lock == 0 && ngx_atomic_cmp_set(mtx->lock, 0, ngx_pid)) {
                // 尝试获取锁  
                (void) ngx_atomic_fetch_add(mtx->wait, -1);
                return;// 如果成功则原子减少等待进程数并返回
            }

            ngx_log_debug1(NGX_LOG_DEBUG_CORE, ngx_cycle->log, 0,
                           "shmtx wait %uA", *mtx->wait);
// 打印等待进程数调试日志
            while (sem_wait(&mtx->sem) == -1) {// 通过信号量sem等待
                ngx_err_t  err;

                err = ngx_errno;

                if (err != NGX_EINTR) { // 如果被信号中断,则继续等待
                    ngx_log_error(NGX_LOG_ALERT, ngx_cycle->log, err,
                                  "sem_wait() failed while waiting on shmtx");
                    break; // 否则打印错误并退出循环
                }
            }

            ngx_log_debug0(NGX_LOG_DEBUG_CORE, ngx_cycle->log, 0,
                           "shmtx awoke");
             // 打印得到锁调试日志
            continue;
        }

#endif

        ngx_sched_yield();// 如果不支持信号量,则调用调度程序释放CPU
    }
}


void
ngx_shmtx_unlock(ngx_shmtx_t *mtx)
{
    if (mtx->spin != (ngx_uint_t) -1) {
        ngx_log_debug0(NGX_LOG_DEBUG_CORE, ngx_cycle->log, 0, "shmtx unlock");
    }

    if (ngx_atomic_cmp_set(mtx->lock, ngx_pid, 0)) {
        ngx_shmtx_wakeup(mtx);
    }
}


ngx_uint_t
ngx_shmtx_force_unlock(ngx_shmtx_t *mtx, ngx_pid_t pid) // 强制解锁mtx,成功返回1,失败返回0
{
    ngx_log_debug0(NGX_LOG_DEBUG_CORE, ngx_cycle->log, 0,
                   "shmtx forced unlock");

    if (ngx_atomic_cmp_set(mtx->lock, pid, 0)) {
        ngx_shmtx_wakeup(mtx);
        return 1;
    }

    return 0;
}


static void
ngx_shmtx_wakeup(ngx_shmtx_t *mtx) // 唤醒等待mtx的进程
{
#if (NGX_HAVE_POSIX_SEM)  // 如果支持 POSIX 信号量
    ngx_atomic_uint_t  wait;

    if (!mtx->semaphore) {
        return; // 如果互斥锁没有使用信号量则返回

    }

    for ( ;; ) {

        wait = *mtx->wait; // 读取等待进程数

        if ((ngx_atomic_int_t) wait <= 0) {
            return; // 如果没有进程在等待则返回

        }

        if (ngx_atomic_cmp_set(mtx->wait, wait, wait - 1)) {
            break; // 原子减少等待进程数
        }
    }

    ngx_log_debug1(NGX_LOG_DEBUG_CORE, ngx_cycle->log, 0,// 打印调试日志
                   "shmtx wake %uA", wait);

    if (sem_post(&mtx->sem) == -1) {// 发送信号量唤醒等待进程
        ngx_log_error(NGX_LOG_ALERT, ngx_cycle->log, ngx_errno,// 如果失败则打印错误日志
                      "sem_post() failed while wake shmtx");
    }

#endif
}


#else


ngx_int_t
ngx_shmtx_create(ngx_shmtx_t *mtx, ngx_shmtx_sh_t *addr, u_char *name)
{
    if (mtx->name) {

        if (ngx_strcmp(name, mtx->name) == 0) {
            mtx->name = name;
            return NGX_OK;
        }

        ngx_shmtx_destroy(mtx);
    }

    mtx->fd = ngx_open_file(name, NGX_FILE_RDWR, NGX_FILE_CREATE_OR_OPEN,
                            NGX_FILE_DEFAULT_ACCESS);

    if (mtx->fd == NGX_INVALID_FILE) {
        ngx_log_error(NGX_LOG_EMERG, ngx_cycle->log, ngx_errno,
                      ngx_open_file_n " \"%s\" failed", name);
        return NGX_ERROR;
    }

    if (ngx_delete_file(name) == NGX_FILE_ERROR) {
        ngx_log_error(NGX_LOG_ALERT, ngx_cycle->log, ngx_errno,
                      ngx_delete_file_n " \"%s\" failed", name);
    }

    mtx->name = name;

    return NGX_OK;
}


void
ngx_shmtx_destroy(ngx_shmtx_t *mtx)
{
    if (ngx_close_file(mtx->fd) == NGX_FILE_ERROR) {
        ngx_log_error(NGX_LOG_ALERT, ngx_cycle->log, ngx_errno,
                      ngx_close_file_n " \"%s\" failed", mtx->name);
    }
}


ngx_uint_t
ngx_shmtx_trylock(ngx_shmtx_t *mtx)
{
    ngx_err_t  err;

    err = ngx_trylock_fd(mtx->fd);

    if (err == 0) {
        return 1;
    }

    if (err == NGX_EAGAIN) {
        return 0;
    }

#if __osf__ /* Tru64 UNIX */

    if (err == NGX_EACCES) {
        return 0;
    }

#endif

    ngx_log_abort(err, ngx_trylock_fd_n " %s failed", mtx->name);

    return 0;
}


void
ngx_shmtx_lock(ngx_shmtx_t *mtx)
{
    ngx_err_t  err;

    err = ngx_lock_fd(mtx->fd);

    if (err == 0) {
        return;
    }

    ngx_log_abort(err, ngx_lock_fd_n " %s failed", mtx->name);
}


void
ngx_shmtx_unlock(ngx_shmtx_t *mtx)
{
    ngx_err_t  err;

    err = ngx_unlock_fd(mtx->fd);

    if (err == 0) {
        return;
    }

    ngx_log_abort(err, ngx_unlock_fd_n " %s failed", mtx->name);
}


ngx_uint_t
ngx_shmtx_force_unlock(ngx_shmtx_t *mtx, ngx_pid_t pid)
{
    return 0;
}

#endif
