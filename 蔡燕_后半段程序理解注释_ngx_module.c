
/*
 * Copyright (C) Igor Sysoev
 * Copyright (C) Maxim Dounin
 * Copyright (C) Nginx, Inc.
 */


#include <ngx_config.h>
#include <ngx_core.h>


#define NGX_MAX_DYNAMIC_MODULES  128


static ngx_uint_t ngx_module_index(ngx_cycle_t *cycle);
static ngx_uint_t ngx_module_ctx_index(ngx_cycle_t *cycle, ngx_uint_t type,
    ngx_uint_t index);


ngx_uint_t         ngx_max_module;
static ngx_uint_t  ngx_modules_n;


ngx_int_t
ngx_preinit_modules(void)
{
    ngx_uint_t  i;

    for (i = 0; ngx_modules[i]; i++) {
        ngx_modules[i]->index = i;
        ngx_modules[i]->name = ngx_module_names[i];
    }

    ngx_modules_n = i;
    ngx_max_module = ngx_modules_n + NGX_MAX_DYNAMIC_MODULES;

    return NGX_OK;
}


ngx_int_t
ngx_cycle_modules(ngx_cycle_t *cycle)
{
    /*
     * create a list of modules to be used for this cycle,
     * copy static modules to it
     */

    cycle->modules = ngx_pcalloc(cycle->pool, (ngx_max_module + 1)
                                              * sizeof(ngx_module_t *));
    if (cycle->modules == NULL) {
        return NGX_ERROR;
    }

    ngx_memcpy(cycle->modules, ngx_modules,
               ngx_modules_n * sizeof(ngx_module_t *));

    cycle->modules_n = ngx_modules_n;

    return NGX_OK;
}


ngx_int_t
ngx_init_modules(ngx_cycle_t *cycle)
{
    ngx_uint_t  i;

    for (i = 0; cycle->modules[i]; i++) {
        if (cycle->modules[i]->init_module) {
            if (cycle->modules[i]->init_module(cycle) != NGX_OK) {
                return NGX_ERROR;
            }
        }
    }

    return NGX_OK;
}


ngx_int_t
ngx_count_modules(ngx_cycle_t *cycle, ngx_uint_t type)
{
    ngx_uint_t     i, next, max;
    ngx_module_t  *module;

    next = 0;
    max = 0;

    /* count appropriate modules, set up their indices */

    for (i = 0; cycle->modules[i]; i++) {
        module = cycle->modules[i];

        if (module->type != type) {
            continue;
        }

        if (module->ctx_index != NGX_MODULE_UNSET_INDEX) {

            /* if ctx_index was assigned, preserve it */

            if (module->ctx_index > max) {
                max = module->ctx_index;
            }

            if (module->ctx_index == next) {
                next++;
            }

            continue;
        }

        /* search for some free index */

        module->ctx_index = ngx_module_ctx_index(cycle, type, next);

        if (module->ctx_index > max) {
            max = module->ctx_index;
        }

        next = module->ctx_index + 1;
    }

    /*
     * make sure the number returned is big enough for previous
     * cycle as well, else there will be problems if the number
     * will be stored in a global variable (as it's used to be)
     * and we'll have to roll back to the previous cycle
     */

    if (cycle->old_cycle && cycle->old_cycle->modules) {

        for (i = 0; cycle->old_cycle->modules[i]; i++) {
            module = cycle->old_cycle->modules[i];

            if (module->type != type) {
                continue;
            }

            if (module->ctx_index > max) {
                max = module->ctx_index;
            }
        }
    }

    /* prevent loading of additional modules */

    cycle->modules_used = 1;

    return max + 1;
}
/*
函数名：ngx_add_module
输入变量：cf(地址)，file(文件名)，module（模块），order（顺序）
返回变量：NGX_OK
*/
ngx_int_t	
ngx_add_module(ngx_conf_t *cf, ngx_str_t *file, ngx_module_t *module,
    char **order)
{
    void               *rv;				
    ngx_uint_t          i, m, before;//定义int变量
    ngx_core_module_t  *core_module;	//定义核心模块

    if (cf->cycle->modules_n >= ngx_max_module) {//如果整个系列中的模块超过最大极限就返回“加载的模块太多”的错误
        ngx_conf_log_error(NGX_LOG_EMERG, cf, 0,
                           "too many modules loaded");
        return NGX_ERROR;
    }

    if (module->version != nginx_version) {//如果模块的版本不是 Nginx 版本就返回"这个文件模块的版本是自己定义的版本而不是Nginx 版本"的错误
        ngx_conf_log_error(NGX_LOG_EMERG, cf, 0,
                           "module \"%V\" version %ui instead of %ui",
                           file, module->version, (ngx_uint_t) nginx_version);
        return NGX_ERROR;
    }

    if (ngx_strcmp(module->signature, NGX_MODULE_SIGNATURE) != 0) {//如果ngx_strcmp字符串比较函数中模块的识别标志部不等于0，就返回" 这个模块不兼容二进制"的错误
        ngx_conf_log_error(NGX_LOG_EMERG, cf, 0,
                           "module \"%V\" is not binary compatible",
                           file);
        return NGX_ERROR;
    }

    for (m = 0; cf->cycle->modules[m]; m++) {//如果整个系列中的模块的名字==0，就返回" 这个模块的名字是已加载"的错误
        if (ngx_strcmp(cf->cycle->modules[m]->name, module->name) == 0) {
            ngx_conf_log_error(NGX_LOG_EMERG, cf, 0,
                               "module \"%s\" is already loaded",
                               module->name);
            return NGX_ERROR;
        }
    }

    /*
     * if the module wasn't previously loaded, assign an index(如果以前没有加载模块，则分配一个索引)
     */

    if (module->index == NGX_MODULE_UNSET_INDEX) {//如果模块的索引等于NGX_MODULE_UNSET_INDEX（模块取消设置索引）
        module->index = ngx_module_index(cf->cycle);//就赋一个模块索引

        if (module->index >= ngx_max_module) {//如果模块的索引超过最大极限就返回“加载的模块太多”的错误
            ngx_conf_log_error(NGX_LOG_EMERG, cf, 0,
                               "too many modules loaded");
            return NGX_ERROR;
        }
    }

    /*
     * put the module into the cycle->modules array（将模块放入系列的模块数组中）
     */

    before = cf->cycle->modules_n;//把系列的模块放到before这个变量中

    if (order) {//判断顺序
        for (i = 0; order[i]; i++) {
            if (ngx_strcmp(order[i], module->name) == 0) {//如果这个顺序数组的模块名字==0；就让i++后跳出for
                i++;
                break;
            }
        }

        for ( /* void */ ; order[i]; i++) {//循环order[i](顺序数组)

#if 0
			//不执行下面三句
            ngx_log_debug2(NGX_LOG_DEBUG_CORE, cf->log, 0,
                           "module: %s before %s",
                           module->name, order[i]);
#endif

            for (m = 0; m < before; m++) {//m小于before就一直循环
                if (ngx_strcmp(cf->cycle->modules[m]->name, order[i]) == 0) {//如果系列模块的名字的顺序==0；就调用调试函数（调试主要部分，模块：名字在顺序之前：m）

                    ngx_log_debug3(NGX_LOG_DEBUG_CORE, cf->log, 0,
                                   "module: %s before %s:%i",
                                   module->name, order[i], m);

                    before = m;	//将m的值赋给before后跳出for
                    break;
                }
            }
        }
    }

    /* put the module before modules[before]（把module放在modules[before]数值中） */

    if (before != cf->cycle->modules_n) {//判断before的值要是不等于modules_n
        ngx_memmove(&cf->cycle->modules[before + 1],//就调用ngx_memmove（）函数包括了&modules[before]数值中的后一位，&modules[before]数值，获取 ngx_module_t 的特定类型占用内存大小
                    &cf->cycle->modules[before],
                    (cf->cycle->modules_n - before) * sizeof(ngx_module_t *));
    }

    cf->cycle->modules[before] = module;	//把module赋值给modules[before]
    cf->cycle->modules_n++;		//modules_n自加

    if (module->type == NGX_CORE_MODULE) {//判断模块类型==NGX_CORE_MODULE

        /*
         * we are smart enough to initialize core modules;
         * other modules are expected to be loaded before
         * initialization - e.g., http modules must be loaded
         * before http{} block
         */

        core_module = module->ctx;//把模块中的ctx赋值给核心模块

        if (core_module->create_conf) {//判断核心模块指向创建初始化
            rv = core_module->create_conf(cf->cycle);//把核心模块指向的创建初始化中的cf系列赋值给rv
            if (rv == NULL) {	//判断rv是否为空，为空返回NGX_ERROR
                return NGX_ERROR;
            }

            cf->cycle->conf_ctx[module->index] = rv;	//不可空就把rv的值赋给conf_ctx[module->index]
        }
    }

    return NGX_OK;	//返回NGX_OK
}

/*
函数名：ngx_module_index
输入变量：*cycle(系列)
返回变量：index
*/
static ngx_uint_t	//定义静态变量ngx_uint_t 
ngx_module_index(ngx_cycle_t *cycle)	
{
    ngx_uint_t     i, index;	//定义int变量i，index（指数）
    ngx_module_t  *module;		//定义模块

    index = 0;	//初始化index
//运用无条件转移语句，终止程序在查找未使用的索引深度嵌套的结构的处理过程。
again:

    /* find an unused index （查找未使用的索引）*/

    for (i = 0; cycle->modules[i]; i++) {//循环系列模块数组i
        module = cycle->modules[i];	//把系列模块数组modules[i]赋值给module

        if (module->index == index) {//判断模块的索引是否等于index变量
            index++;	//等于就index自加
            goto again;
        }
    }

    /* check previous cycle (检查上一个系列)*/

    if (cycle->old_cycle && cycle->old_cycle->modules) {//如果旧的系列&&旧的系列模块

        for (i = 0; cycle->old_cycle->modules[i]; i++) {//循环cycle->old_cycle->modules[i]次
            module = cycle->old_cycle->modules[i];	//把旧的系列模块赋值给module

            if (module->index == index) {	//如果索引变量等于模块索引，索引变量自加
                index++;
                goto again;	
            }
        }
    }

    return index;	//返回这个索引变量
}
/*
ngx_module_ctx_index
输入变量：*cycle(系列),type(类型)，index（索引）
返回变量：index
*/
static ngx_uint_t//定义静态变量ngx_uint_t 
ngx_module_ctx_index(ngx_cycle_t *cycle, ngx_uint_t type, ngx_uint_t index)
{
    ngx_uint_t     i;
    ngx_module_t  *module;
//运用无条件转移语句，终止程序在查找未使用的索引深度嵌套的结构的处理过程。
again:	

    /* find an unused ctx_index （查找未使用的ctx索引）*/

    for (i = 0; cycle->modules[i]; i++) {
        module = cycle->modules[i];

        if (module->type != type) {		//如果模块类型不等于type变量，跳出本次循环
            continue;
        }

        if (module->ctx_index == index) {//如果ctx索引等于 index 变量，index自加
            index++;
            goto again;
        }
    }

    /* check previous cycle(检查上一个系列) */

    if (cycle->old_cycle && cycle->old_cycle->modules) {//如果旧的系列&&旧的系列模块

        for (i = 0; cycle->old_cycle->modules[i]; i++) {//循环查找cycle->old_cycle->modules[i]次
            module = cycle->old_cycle->modules[i];//把旧的系列模块赋值给module

            if (module->type != type) {//如果模块类型不等于type变量，跳出本次循环
                continue;
            }

            if (module->ctx_index == index) {//如果ctx索引等于 index 变量，index自加
                index++;
                goto again;
            }
        }
    }
    return index;	//返回这个索引变量
}
