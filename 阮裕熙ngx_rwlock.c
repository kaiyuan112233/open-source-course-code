
/*
 * Copyright (C) Ruslan Ermilov
 * Copyright (C) Nginx, Inc.
 */


#include <ngx_config.h>
#include <ngx_core.h>


#if (NGX_HAVE_ATOMIC_OPS)


#define NGX_RWLOCK_SPIN   2048
#define NGX_RWLOCK_WLOCK  ((ngx_atomic_uint_t) -1)


void
ngx_rwlock_wlock(ngx_atomic_t *lock)
{
    ngx_uint_t  i, n;

    for ( ;; ) {#无限循环的开始

        if (*lock == 0 && ngx_atomic_cmp_set(lock, 0, NGX_RWLOCK_WLOCK)) {#如果锁没有被占用，那么就尝试使用原子操作将锁设置为写锁。如果设置成功，就返回。
            return;
        }

        if (ngx_ncpu > 1) {#如果 CPU 的数量大于 1，那么就进入下面的循环。

            for (n = 1; n < NGX_RWLOCK_SPIN; n <<= 1) {

                for (i = 0; i < n; i++) {
                    ngx_cpu_pause();
                }

                if (*lock == 0
                    && ngx_atomic_cmp_set(lock, 0, NGX_RWLOCK_WLOCK))#如果锁没有被占用，那么就尝试使用原子操作将锁设置为写锁。如果设置成功，就返回。
                {
                    return;
                }
            }
        }

        ngx_sched_yield();
    }
}
#读写锁函数

void
ngx_rwlock_rlock(ngx_atomic_t *lock)
{
    ngx_uint_t         i, n;
    ngx_atomic_uint_t  readers;

    for ( ;; ) {
        readers = *lock;

        if (readers != NGX_RWLOCK_WLOCK#如果锁没有被占用，那么就尝试使用原子操作将锁设置为读锁。如果设置成功，就返回。
            && ngx_atomic_cmp_set(lock, readers, readers + 1))
        {
            return;
        }

        if (ngx_ncpu > 1) {#如果 CPU 的数量大于 1，那么就进入下面的循环

            for (n = 1; n < NGX_RWLOCK_SPIN; n <<= 1) {

                for (i = 0; i < n; i++) {
                    ngx_cpu_pause();#每次迭代中调用 ngx_cpu_pause() 函数
                }

                readers = *lock;
                #如果锁没有被占用，那么就尝试使用原子操作将锁设置为读锁。如果设置成功，就返回。

                if (readers != NGX_RWLOCK_WLOCK
                    && ngx_atomic_cmp_set(lock, readers, readers + 1))
                {
                    return;
                }
            }
        }

        ngx_sched_yield();#让出 CPU 时间片
    }
}

#
void
ngx_rwlock_unlock(ngx_atomic_t *lock)
{
    if (*lock == NGX_RWLOCK_WLOCK) {#如果锁被设置为写锁，那么就进入下面的语句块。
        (void) ngx_atomic_cmp_set(lock, NGX_RWLOCK_WLOCK, 0);#使用原子操作将锁设置为未占用状态。
    } else {
        (void) ngx_atomic_fetch_add(lock, -1);#使用原子操作将锁的计数器减 1
    }
}

#用于实现读写锁的函数
void
ngx_rwlock_downgrade(ngx_atomic_t *lock)
{
    if (*lock == NGX_RWLOCK_WLOCK) {#如果锁被设置为写锁，那么就进入下面的语句块。
        *lock = 1;#将锁设置为读锁
    }
}


#else

#if (NGX_HTTP_UPSTREAM_ZONE || NGX_STREAM_UPSTREAM_ZONE)

#error ngx_atomic_cmp_set() is not defined!

#endif

#endif
